import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminPanelComponent } from "./core/layout/components/admin-panel.component";
import { AuthGuard } from "./core/services/auth.guard/auth.guard";

const routes: Routes = [
  {
    path: "login",
    loadChildren: "./modules/login/login.module#LoginModule",
  },
  {
    path: "",
    component: AdminPanelComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "", redirectTo: "admin", pathMatch: "full" },
      {
        path: "admin",
        loadChildren: "./modules/admin/admin.module#AdminModule",
      },
      {
        path: "teacher",
        loadChildren: "./modules/teacher/teacher.module#TeacherModule",
      },
      {
        path: "student",
        loadChildren: "./modules/student/student.module#StudentModule",
      },
      {
        path: "assessment",
        loadChildren: "./modules/assessment/assessment.module#AssessmentModule",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
