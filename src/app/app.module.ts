import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { CoreModule } from "./core/core.module";
import { HttpClientModule } from "@angular/common/http";
import { ModalModule } from "ngx-bootstrap";
import { ToastrModule } from "ngx-toastr";
import { ConfirmationDialogComponent } from "./modules/shared/components/confirmation-dialog/confirmation-dialog.component";
import { SharedModule } from "./modules/shared/shared.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent],
})
export class AppModule {}
platformBrowserDynamic().bootstrapModule(AppModule);
