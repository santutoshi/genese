import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSidenav } from "@angular/material";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToastrService } from "ngx-toastr";
import { ConfirmationDialogComponent } from "src/app/modules/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Student } from "src/app/modules/student/models/student.models";
import { Admin } from "../../models/admin.models";
import { AdminService } from "../../services/admin.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit {
  @ViewChild("AddEditStudentFormRef", { static: true }) AddEditStudentFormRef;
  @ViewChild("AddEditTeacherFormRef", { static: true }) AddEditTeacherFormRef;

  displayedColumns: string[] = ["id", "name", "class", "action"];
  displayedStudentColumns: string[] = ["id", "name", "score", "action"];

  teacherForm: FormGroup;
  studentForm: FormGroup;
  submitted: boolean;
  editMode: boolean;

  modalStudentTitle: any;
  modalTeacherTitle: any;
  dataSource: Admin[] = [];
  dataSourceStudent: Student[] = [];
  modalRef: BsModalRef;
  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };

  constructor(
    private adminService: AdminService,
    private modalService: BsModalService,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getTeacherList();
    this.getStudentList();
  }

  getTeacherList(): void {
    this.adminService.getTeacher().subscribe((response) => {
      this.dataSource = response;
    });
  }

  getStudentList(): void {
    this.adminService.getStudent().subscribe((response) => {
      this.dataSourceStudent = response;
    });
  }

  buildTeacherForm(): void {
    this.teacherForm = this._fb.group({
      id: null,
      name: ["", Validators.required],
      class: ["", Validators.required],
    });
  }

  buildStudentForm(): void {
    this.studentForm = this._fb.group({
      id: null,
      name: ["", Validators.required],
      score: ["", Validators.required],
    });
  }

  get ft() {
    return this.teacherForm.controls;
  }

  get fs() {
    return this.studentForm.controls;
  }

  teacherUpdate(): void {
    this.submitted = true;
    if (this.teacherForm.invalid) return;

    this.adminService
      .updateTeacher(this.teacherForm.value.id, this.teacherForm.value)
      .subscribe((response) => {
        this.toastr.success("Teacher edited Successfully");
        this.modalRef.hide();
        this.submitted = false;
        this.getTeacherList();
      });
  }

  onTeacherSave(): void {
    this.submitted = true;
    if (this.teacherForm.invalid) return;

    this.adminService
      .postTeacher(this.teacherForm.value)
      .subscribe((response) => {
        this.toastr.success("Teacher added Successfully");
        this.modalRef.hide();
        this.submitted = false;
        this.getTeacherList();
      });
  }

  studentUpdate(): void {
    this.submitted = true;
    if (this.studentForm.invalid) return;

    this.adminService
      .updateStudent(this.studentForm.value.id, this.studentForm.value)
      .subscribe((response) => {
        this.toastr.success("Student edited Successfully");
        this.modalRef.hide();
        this.submitted = false;
        this.getStudentList();
      });
  }

  onStudentSave(): void {
    this.submitted = true;
    if (this.studentForm.invalid) return;

    this.adminService
      .postStudent(this.studentForm.value)
      .subscribe((response) => {
        this.toastr.success("Student added Successfully");
        this.modalRef.hide();
        this.submitted = false;
        this.getStudentList();
      });
  }

  rowSelect(element): void {
    this.editMode = true;
    this.submitted = false;
    this.modalTeacherTitle = "Edit Teacher";
    this.modalRef = this.modalService.show(
      this.AddEditTeacherFormRef,
      this.config
    );
    this.buildTeacherForm();
    this.teacherForm.patchValue(element);
  }

  deleteSelect(element): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = element.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        this.deleteTeacherByID(element.id);
      }
    });
  }

  deleteTeacherByID(id): void {
    this.adminService.deleteTeacher(id).subscribe((response) => {
      this.toastr.success("Teacher deleted Successfully");
      this.getTeacherList();
    });
  }

  onCancel(): void {
    this.submitted = false;
    this.editMode = false;
    this.modalRef.hide();
  }

  rowSelectStudent(element): void {
    this.editMode = true;
    this.submitted = false;
    this.modalStudentTitle = "Edit Student";
    this.modalRef = this.modalService.show(
      this.AddEditStudentFormRef,
      this.config
    );
    this.buildStudentForm();
    this.studentForm.patchValue(element);
    console.log("Element" + JSON.stringify(element));
  }

  deleteSelectStudent(element): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = element.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        this.deleteStudentByID(element.id);
      }
    });
  }

  deleteStudentByID(id): void {
    this.adminService.deleteStudent(id).subscribe((response) => {
      this.toastr.success("Student deleted Successfully");
      this.getStudentList();
    });
  }

  openStudentModal(template: TemplateRef<any>) {
    this.modalStudentTitle = "Add Student";
    this.modalRef = this.modalService.show(template, this.config);
    this.buildStudentForm();
  }

  openTeacherModal(template: TemplateRef<any>) {
    this.modalTeacherTitle = "Add Teacher";
    this.modalRef = this.modalService.show(template, this.config);
    this.buildTeacherForm();
  }
}
