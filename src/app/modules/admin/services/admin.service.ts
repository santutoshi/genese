import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { from, Observable } from "rxjs";
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class AdminService {
  apiURL: any = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getTeacher(): Observable<any> {
    return this.http.get(`${this.apiURL}teachers`);
  }

  getTeacherByID(id) {
    return this.http.get(`${this.apiURL}teachers/${id}`);
  }

  getStudent(): Observable<any> {
    return this.http.get(`${this.apiURL}students`);
  }

  postStudent(body): Observable<any> {
    return this.http.post(`${this.apiURL}students`, body);
  }

  deleteStudent(studentId): Observable<any> {
    return this.http.delete(`${this.apiURL}students/${studentId}`);
  }

  updateStudent(studentId, body): Observable<any> {
    return this.http.put(`${this.apiURL}students/${studentId}`, body);
  }

  postTeacher(body): Observable<any> {
    return this.http.post(`${this.apiURL}teachers`, body);
  }

  updateTeacher(teacherId, body): Observable<any> {
    return this.http.put(`${this.apiURL}teachers/${teacherId}`, body);
  }

  deleteTeacher(teacherId): Observable<any> {
    return this.http.delete(`${this.apiURL}teachers/${teacherId}`);
  }
}
