import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import {
  Validators,
  FormBuilder,
  FormGroup,
  AbstractControl,
} from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  loginForm: FormGroup;
  submitted: boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _fb: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    this.buildLoginForm();
  }

  buildLoginForm() {
    this.loginForm = this._fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }
  login(): void {
    this.submitted = true;

    if (this.loginForm.invalid) return;
    if (
      this.loginForm.value.username == "admin" &&
      this.loginForm.value.password == "admin"
    ) {
      this.toastr.success("Login Successfully");
      localStorage.setItem("token", "TERROR");
      this.router.navigate([""]);
    } else {
      this.toastr.error("Username and Password dosen't match");
    }
  }

  // form controls
  get username(): AbstractControl {
    return this.loginForm.get("username");
  }

  get password(): AbstractControl {
    return this.loginForm.get("password");
  }
}
