import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToastrService } from "ngx-toastr";
import { Assessment } from "src/app/modules/assessment/models/assessment.models";
import { ConfirmationDialogComponent } from "src/app/modules/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Student } from "src/app/modules/student/models/student.models";
import { Teacher } from "../../models/teacher.models";
import { TeacherService } from "../../services/teacher.service";
@Component({
  selector: "app-teacher",
  templateUrl: "./teacher.component.html",
  styleUrls: ["./teacher.component.scss"],
})
export class TeacherComponent implements OnInit {
  @ViewChild("AddEditFormRef", { static: true }) AddEditFormRef;
  @ViewChild("Verify", { static: true }) Verify;
  assessmentForm: FormGroup;
  displayedColumns: string[] = [
    "id",
    "student",
    "score",
    "startDate",
    "submittedDate",
    "verified",
    "action",
  ];
  modalRef: BsModalRef;
  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  editMode: boolean = false;
  submittedDateYes: boolean = false;
  modalTitle: any;

  dataSource: Assessment[] = [];
  teacherID: any;
  teachers: Teacher[] = [];
  students: Student[] = [];
  constructor(
    private _fb: FormBuilder,
    public teacherService: TeacherService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getTeachers();
    this.getStudent();
  }

  getTeachers(): void {
    this.teacherService.getTeachers().subscribe((response) => {
      this.teachers = response;
    });
  }

  getStudent(): void {
    this.teacherService.getStudents().subscribe((respone) => {
      this.students = respone;
    });
  }

  teacherChange(value) {
    this.getAssessment(value);
  }

  getAssessment(value): void {
    this.teacherService.getAssessment(value).subscribe((response) => {
      this.dataSource = response;
    });
  }

  buildAssigmentFormGroup(): void {
    this.assessmentForm = this._fb.group({
      id: [null],
      student: [null, [Validators.required]],
      score: [0],
      startDate: ["", [Validators.required]],
      submittedDate: [""],
      verified: [false],
      teacherId: [null],
      studentId: [null],
    });
  }

  get f() {
    return this.assessmentForm.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalTitle = "Add Assessment";
    this.modalRef = this.modalService.show(template, this.config);
    this.buildAssigmentFormGroup();
  }

  onCancel(): void {
    this.submitted = false;
    this.modalRef.hide();
  }

  rowSelect(element): void {
    this.editMode = true;
    this.buildAssigmentFormGroup();
    this.assessmentForm.patchValue(element);
    this.modalTitle = "Edit Assessment";
    this.modalRef = this.modalService.show(this.AddEditFormRef, this.config);
  }

  verify(element): void {
    this.editMode = true;
    this.buildAssigmentFormGroup();
    this.assessmentForm.patchValue(element);

    if (!this.assessmentForm.value.submittedDate) {
      this.toastr.warning("Submitted Date is not available ");
      // this.modalRef = this.modalService.show(this.Verify, this.config);
    } else {
      this.assessmentForm.get("verified").setValue(true);
      this.teacherService
        .updateAssessment(
          this.assessmentForm.value.id,
          this.assessmentForm.value
        )
        .subscribe((response) => {
          this.toastr.success("Assessment Verified");
          this.getAssessment(this.teacherID);
        });
    }
  }

  deleteSelect(element): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = element.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        this.deleteAssessmentByID(element.id);
      }
    });
  }

  deleteAssessmentByID(id): void {
    this.teacherService.deleteAssessment(id).subscribe((response) => {
      this.toastr.success("Assessment deleted Successfully");
      this.getAssessment(this.teacherID);
    });
  }

  clearModal(): void {
    this.submitted = false;

    this.assessmentForm.reset();
    this.getAssessment(this.teacherID);
  }

  submitted: boolean = false;
  onSave(): void {
    this.submitted = true;
    this.assessmentForm
      .get("studentId")
      .setValue(this.assessmentForm.value.student);
    this.assessmentForm.get("teacherId").setValue(parseInt(this.teacherID));

    if (this.assessmentForm.invalid) return;
    this.teacherService
      .postAssessment(this.assessmentForm.value)
      .subscribe((response) => {
        this.toastr.success("Assessment has added Successfully");
        this.onCancel();
        this.getAssessment(this.teacherID);
      });
  }

  submittedDateChange(): void {
    this.submittedDateYes = true;
  }

  update(): void {
    this.submitted = true;

    if (this.assessmentForm.invalid) return;
    this.teacherService
      .updateAssessment(this.assessmentForm.value.id, this.assessmentForm.value)
      .subscribe((response) => {
        this.toastr.success("Assessment edited successfully");
        this.onCancel();
        this.getAssessment(this.teacherID);
      });
  }
}
