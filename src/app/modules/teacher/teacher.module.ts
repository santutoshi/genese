import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeacherRoutingModule } from "./teacher-routing.module";
import { TeacherComponent } from "./components/teacher/teacher.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "src/app/core/material/material.module";
import { SharedModule } from "../shared/shared.module";
import { ConfirmationDialogComponent } from "../shared/components/confirmation-dialog/confirmation-dialog.component";

@NgModule({
  declarations: [TeacherComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    TeacherRoutingModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  // entryComponents: [ConfirmationDialogComponent],
})
export class TeacherModule {}
