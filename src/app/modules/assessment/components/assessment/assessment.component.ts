import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToastrService } from "ngx-toastr";
import { ConfirmationDialogComponent } from "src/app/modules/shared/components/confirmation-dialog/confirmation-dialog.component";
import { Student } from "src/app/modules/student/models/student.models";
import { Teacher } from "src/app/modules/teacher/models/teacher.models";
import { Assessment } from "../../models/assessment.models";
import { AssessmentService } from "../../services/assessment.service";

@Component({
  selector: "app-assessment",
  templateUrl: "./assessment.component.html",
  styleUrls: ["./assessment.component.scss"],
})
export class AssessmentComponent implements OnInit {
  @ViewChild("AddEditFormRef", { static: true }) AddEditFormRef;

  displayedColumns: string[] = [
    "id",
    "student",
    "teacher",
    "score",
    "startDate",
    "submittedDate",
    "verified",
    "action",
  ];
  assessmentForm: FormGroup;
  modalRef: BsModalRef;
  submitted: boolean;
  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  teachers: Teacher[] = [];
  students: Student[] = [];
  dataSource: Assessment[] = [];
  constructor(
    public assessmentService: AssessmentService,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.getStudent();
    this.getTeachers();
    this.getAssessment();
  }

  buildAssessmentFormGroup() {
    this.assessmentForm = this._fb.group({
      id: [null],
      student: [null],
      score: [0],
      startDate: [""],
      submittedDate: [""],
      verified: [false],
      teacherId: [null],
      studentId: [null],
    });
  }
  get f() {
    return this.assessmentForm.controls;
  }
  getTeachers(): void {
    this.assessmentService.getTeachers().subscribe((response) => {
      this.teachers = response;
    });
  }

  getStudent(): void {
    this.assessmentService.getStudents().subscribe((respone) => {
      this.students = respone;
    });
  }

  getAssessment(): void {
    this.assessmentService.getAssessment().subscribe((response) => {
      this.dataSource = response;
    });
  }
  editAssigment(element): void {
    this.buildAssessmentFormGroup();
    this.assessmentForm.patchValue(element);
    this.modalRef = this.modalService.show(this.AddEditFormRef, this.config);
  }

  deleteAssigment(element): void {
    this.modalRef = this.modalService.show(
      ConfirmationDialogComponent,
      this.config
    );
    this.modalRef.content.data = element.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        this.deleteAssessmentByID(element.id);
      }
    });
  }

  deleteAssessmentByID(id): void {
    this.assessmentService.deleteAssessment(id).subscribe((response) => {
      this.toastr.success("Assessment deleted Successfully");
      this.getAssessment();
    });
  }

  onCancel(): void {
    this.submitted = false;
    this.modalRef.hide();
  }
  update(): void {
    this.submitted = true;
    this.assessmentForm
      .get("studentId")
      .setValue(this.assessmentForm.value.student);
    if (this.assessmentForm.invalid) return;
    this.assessmentService
      .updateAssessment(this.assessmentForm.value.id, this.assessmentForm.value)
      .subscribe((response) => {
        this.toastr.success("Assessment edited successfully");
        this.onCancel();
        this.getAssessment();
      });
  }
}
