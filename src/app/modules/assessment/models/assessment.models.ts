export interface Assessment {
  id: number;
  student: number;
  score: string;
  startDate: Date;
  submittedDate: Date;
  verified: boolean;
}
