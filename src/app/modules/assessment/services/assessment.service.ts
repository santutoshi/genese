import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class AssessmentService {
  apiURL = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getStudents(): Observable<any> {
    return this.http.get(`${this.apiURL}students`);
  }

  getTeachers(): Observable<any> {
    return this.http.get(`${this.apiURL}teachers`);
  }

  getAssessment(): Observable<any> {
    return this.http.get(`${this.apiURL}assessments`);
  }

  deleteAssessment(assessmentId): Observable<any> {
    return this.http.delete(`${this.apiURL}assessments/${assessmentId}`);
  }

  updateAssessment(assessmentId, body): Observable<any> {
    return this.http.put(`${this.apiURL}assessments/${assessmentId}`, body);
  }
}
