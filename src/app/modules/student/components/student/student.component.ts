import { DatePipe, formatDate } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ToastrService } from "ngx-toastr";
import { Assessment } from "src/app/modules/assessment/models/assessment.models";
import { Teacher } from "src/app/modules/teacher/models/teacher.models";
import { Student } from "../../models/student.models";
import { StudentService } from "../../services/student.service";

@Component({
  selector: "app-student",
  templateUrl: "./student.component.html",
  styleUrls: ["./student.component.scss"],
})
export class StudentComponent implements OnInit {
  @ViewChild("AddEditFormRef", { static: true }) AddEditFormRef;
  assessmentForm: FormGroup;

  displayedColumns: string[] = [
    "id",
    "student",
    "score",
    "startDate",
    "submittedDate",
    "verified",
    "action",
  ];

  date: Date = new Date();
  modalRef: BsModalRef;
  submitted: boolean;
  // modal config to unhide modal when clicked outside
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };
  studentId: number;
  teachers: Teacher[] = [];
  students: Student[] = [];
  dataSource: Assessment[] = [];

  constructor(
    private _fb: FormBuilder,
    public studentService: StudentService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getTeachers();
    this.getStudent();
  }

  getTeachers(): void {
    this.studentService.getTeachers().subscribe((response) => {
      this.teachers = response;
    });
  }

  getStudent(): void {
    this.studentService.getStudents().subscribe((respone) => {
      this.students = respone;
    });
  }

  studentChange(value) {
    this.getAssessment(value);
  }

  get f() {
    return this.assessmentForm.controls;
  }

  buildAssigmentFormGroup(): void {
    this.assessmentForm = this._fb.group({
      id: [null],
      student: [null, [Validators.required]],
      score: [0, [Validators.required]],
      startDate: ["", [Validators.required]],
      submittedDate: [""],
      verified: [false],
      teacherId: [null],
      studentId: [null],
    });
  }
  getAssessment(value): void {
    this.studentService.getAssessment(value).subscribe((response) => {
      this.dataSource = response;
    });
  }

  datePipe = new DatePipe("en-US"); // Use your own locale

  validDate: boolean;

  rowSelect(element): void {
    const now = Date.now();

    const settoday = this.datePipe.transform(now, "dd/MM/yyyy");
    const startDate = this.datePipe.transform(element.startDate, "dd/MM/yyyy");

    if (settoday > startDate) {
      this.toastr.warning("Starting date of the assessment has been expired");
    } else {
      this.validDate = true;
      this.modalRef = this.modalService.show(this.AddEditFormRef, this.config);
      this.buildAssigmentFormGroup();
      this.assessmentForm.patchValue(element);
    }
  }

  onCancel(): void {
    this.submitted = false;
    this.modalRef.hide();
  }

  update(): void {
    const now = Date.now();

    const settoday = this.datePipe.transform(now, "dd/MM/yyyy");
    this.assessmentForm.get("submittedDate").setValue(settoday);
    if (this.assessmentForm.invalid) return;
    this.studentService
      .updateAssessment(this.assessmentForm.value.id, this.assessmentForm.value)
      .subscribe((response) => {
        this.toastr.success(
          "Assessment Sumitted date has been successfully updated"
        );
        this.getAssessment(this.studentId);
        this.onCancel();
      });
  }
}
