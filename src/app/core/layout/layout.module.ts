import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminPanelComponent } from "./components/admin-panel.component";
import { MaterialModule } from "../material/material.module";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [AdminPanelComponent],
  imports: [CommonModule, RouterModule, MaterialModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LayoutModule {}
